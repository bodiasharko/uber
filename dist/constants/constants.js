"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.maxPasswordLength = exports.minPasswordLength = exports.maxLargeStraightPayload = exports.maxLargeStraightHeight = exports.maxLargeStraightLength = exports.maxLargeStraightWidth = exports.maxSmallStraightPayload = exports.maxSmallStraightHeight = exports.maxSmallStraightLength = exports.maxSmallStraightWidth = exports.maxSprinterPayload = exports.maxSprinterHeight = exports.maxSprinterLength = exports.maxSprinterWidth = exports.finishLoad = exports.passwordGenerated = exports.truckAlreadyAssigned = exports.notNewLoad = exports.notAssigned = exports.driverAssigned = exports.noDriver = exports.tooLarge = exports.loadDeleted = exports.loadUpdated = exports.noLoad = exports.noChange = exports.notUserLoad = exports.noPermission = exports.loadCreated = exports.truckAssigned = exports.truckDeleted = exports.truckChanged = exports.noUserLoad = exports.noUserTruck = exports.noTruck = exports.truckCreated = exports.invalidValidations = exports.profileDeleted = exports.passwordChanged = exports.matchPassword = exports.unAuth = exports.badPassword = exports.noUser = exports.userExist = exports.serverError = exports.success = exports.notPassword = exports.notEmail = exports.notString = void 0;
exports.notString = 'Fields must be string';
exports.notEmail = 'Incorrect E-mail';
exports.notPassword = 'Incorrect Password';
exports.success = 'Success';
exports.serverError = 'Internal server error';
exports.userExist = 'User with this name has already exist';
exports.noUser = 'No user with this email';
exports.badPassword = 'Incorrect password';
exports.unAuth = 'User not authorized';
exports.matchPassword = 'Old password not match';
exports.passwordChanged = 'Password changed successfully';
exports.profileDeleted = 'Profile deleted successfully';
exports.invalidValidations = 'Validation failed';
exports.truckCreated = 'Truck created successfully';
exports.noTruck = 'Truck with this id not exist';
exports.noUserTruck = 'User don`t have assigned trucks';
exports.noUserLoad = 'User don`t have any loads';
exports.truckChanged = 'Truck details changed successfully';
exports.truckDeleted = 'Truck deleted successfully';
exports.truckAssigned = 'Truck assigned successfully';
exports.loadCreated = 'Load created successfully';
exports.noPermission = 'You don`t have permission';
exports.notUserLoad = 'This load not yours';
exports.noChange = `This load not 'NEW'`;
exports.noLoad = 'Load with this ID not exist';
exports.loadUpdated = 'Load details changed successfully';
exports.loadDeleted = 'Load deleted successfully';
exports.tooLarge = 'Too large load';
exports.noDriver = 'Driver not found';
exports.driverAssigned = 'Load posted successfully';
exports.notAssigned = 'Loads not assigned';
exports.notNewLoad = 'This load has already posted';
exports.truckAlreadyAssigned = 'This truck is assigned';
exports.passwordGenerated = 'New password sent to your email address';
exports.finishLoad = 'You can`t reassign truck while you have active load';
exports.maxSprinterWidth = 300;
exports.maxSprinterLength = 250;
exports.maxSprinterHeight = 170;
exports.maxSprinterPayload = 1700;
exports.maxSmallStraightWidth = 500;
exports.maxSmallStraightLength = 250;
exports.maxSmallStraightHeight = 170;
exports.maxSmallStraightPayload = 2500;
exports.maxLargeStraightWidth = 700;
exports.maxLargeStraightLength = 350;
exports.maxLargeStraightHeight = 200;
exports.maxLargeStraightPayload = 4000;
exports.minPasswordLength = 4;
exports.maxPasswordLength = 16;
//# sourceMappingURL=constants.js.map