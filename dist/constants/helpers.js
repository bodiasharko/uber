"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatFullInfoLoad = exports.formatLoad = exports.findTruckType = void 0;
const enums_1 = require("./enums");
const constants_1 = require("./constants");
const findTruckType = (dimensions, payload) => {
    let types = [
        enums_1.EnumTruckType.SPRINTER,
        enums_1.EnumTruckType.LARGE_STRAIGHT,
        enums_1.EnumTruckType.SMALL_STRAIGHT,
    ];
    if (payload > constants_1.maxSprinterPayload ||
        dimensions.width > constants_1.maxSprinterWidth ||
        dimensions.length > constants_1.maxSprinterLength ||
        dimensions.height > constants_1.maxSprinterHeight) {
        types = types.filter((type) => type !== enums_1.EnumTruckType.SPRINTER);
    }
    if (payload > constants_1.maxSmallStraightPayload ||
        dimensions.width > constants_1.maxSmallStraightWidth ||
        dimensions.length > constants_1.maxSmallStraightLength ||
        dimensions.height > constants_1.maxSmallStraightHeight) {
        types = types.filter((type) => type !== enums_1.EnumTruckType.SMALL_STRAIGHT);
    }
    return types;
};
exports.findTruckType = findTruckType;
const formatLoad = ({ _id, created_by, assigned_to, status, state, name, payload, pickup_address, delivery_address, dimensions, logs, createdAt, }) => {
    return {
        _id,
        created_by,
        assigned_to,
        status,
        state,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        logs,
        created_date: createdAt,
    };
};
exports.formatLoad = formatLoad;
const formatFullInfoLoad = (load, { _id, created_by, assigned_to, type, status, createdAt }) => {
    return {
        load: (0, exports.formatLoad)(load),
        truck: {
            _id,
            created_by,
            assigned_to,
            type,
            status,
            created_date: createdAt,
        },
    };
};
exports.formatFullInfoLoad = formatFullInfoLoad;
//# sourceMappingURL=helpers.js.map