export declare enum EnumStatus {
    NEW = "NEW",
    POSTED = "POSTED",
    ASSIGNED = "ASSIGNED",
    SHIPPED = "SHIPPED"
}
export declare enum EnumState {
    ROUTE_TO = "En route to Pick Up",
    ARRIVED = "Arrived to Pick Up",
    DELIVERY = "En route to delivery",
    FINISH = "Arrived to delivery"
}
export declare enum EnumRole {
    SHIPPER = "SHIPPER",
    DRIVER = "DRIVER"
}
export declare enum EnumTruckType {
    SPRINTER = "SPRINTER",
    SMALL_STRAIGHT = "SMALL STRAIGHT",
    LARGE_STRAIGHT = "LARGE STRAIGHT"
}
export declare enum EnumTruckStatus {
    IS = "IS",
    OL = "OL"
}
