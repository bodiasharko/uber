import { LoadDocument } from '../schemas/load.schema';
export declare const findTruckType: (dimensions: any, payload: any) => string[];
export declare const formatLoad: ({ _id, created_by, assigned_to, status, state, name, payload, pickup_address, delivery_address, dimensions, logs, createdAt, }: any | LoadDocument) => {
    _id: any;
    created_by: any;
    assigned_to: any;
    status: any;
    state: any;
    name: any;
    payload: any;
    pickup_address: any;
    delivery_address: any;
    dimensions: any;
    logs: any;
    created_date: any;
};
export declare const formatFullInfoLoad: (load: any, { _id, created_by, assigned_to, type, status, createdAt }: {
    _id: any;
    created_by: any;
    assigned_to: any;
    type: any;
    status: any;
    createdAt: any;
}) => {
    load: {
        _id: any;
        created_by: any;
        assigned_to: any;
        status: any;
        state: any;
        name: any;
        payload: any;
        pickup_address: any;
        delivery_address: any;
        dimensions: any;
        logs: any;
        created_date: any;
    };
    truck: {
        _id: any;
        created_by: any;
        assigned_to: any;
        type: any;
        status: any;
        created_date: any;
    };
};
