"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnumTruckStatus = exports.EnumTruckType = exports.EnumRole = exports.EnumState = exports.EnumStatus = void 0;
var EnumStatus;
(function (EnumStatus) {
    EnumStatus["NEW"] = "NEW";
    EnumStatus["POSTED"] = "POSTED";
    EnumStatus["ASSIGNED"] = "ASSIGNED";
    EnumStatus["SHIPPED"] = "SHIPPED";
})(EnumStatus = exports.EnumStatus || (exports.EnumStatus = {}));
var EnumState;
(function (EnumState) {
    EnumState["ROUTE_TO"] = "En route to Pick Up";
    EnumState["ARRIVED"] = "Arrived to Pick Up";
    EnumState["DELIVERY"] = "En route to delivery";
    EnumState["FINISH"] = "Arrived to delivery";
})(EnumState = exports.EnumState || (exports.EnumState = {}));
var EnumRole;
(function (EnumRole) {
    EnumRole["SHIPPER"] = "SHIPPER";
    EnumRole["DRIVER"] = "DRIVER";
})(EnumRole = exports.EnumRole || (exports.EnumRole = {}));
var EnumTruckType;
(function (EnumTruckType) {
    EnumTruckType["SPRINTER"] = "SPRINTER";
    EnumTruckType["SMALL_STRAIGHT"] = "SMALL STRAIGHT";
    EnumTruckType["LARGE_STRAIGHT"] = "LARGE STRAIGHT";
})(EnumTruckType = exports.EnumTruckType || (exports.EnumTruckType = {}));
var EnumTruckStatus;
(function (EnumTruckStatus) {
    EnumTruckStatus["IS"] = "IS";
    EnumTruckStatus["OL"] = "OL";
})(EnumTruckStatus = exports.EnumTruckStatus || (exports.EnumTruckStatus = {}));
//# sourceMappingURL=enums.js.map