import { EnumTruckType } from '../constants/enums';
export declare class TruckTypeDto {
    type: EnumTruckType.SPRINTER | EnumTruckType.SMALL_STRAIGHT | EnumTruckType.LARGE_STRAIGHT;
}
