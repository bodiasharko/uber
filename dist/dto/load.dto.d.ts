export declare class Dimensions {
    width: number;
    length: number;
    height: number;
}
export declare class LoadDto {
    name: string;
    payload: number;
    pickup_address: string;
    delivery_address: string;
    dimensions: Dimensions;
}
