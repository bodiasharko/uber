"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadDto = exports.Dimensions = void 0;
const class_validator_1 = require("class-validator");
const constants_1 = require("../constants/constants");
class Dimensions {
}
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Max)(constants_1.maxLargeStraightWidth, { message: constants_1.tooLarge }),
    __metadata("design:type", Number)
], Dimensions.prototype, "width", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Max)(constants_1.maxLargeStraightLength, { message: constants_1.tooLarge }),
    __metadata("design:type", Number)
], Dimensions.prototype, "length", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Max)(constants_1.maxLargeStraightHeight, { message: constants_1.tooLarge }),
    __metadata("design:type", Number)
], Dimensions.prototype, "height", void 0);
exports.Dimensions = Dimensions;
class LoadDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], LoadDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Max)(constants_1.maxLargeStraightPayload, { message: constants_1.tooLarge }),
    __metadata("design:type", Number)
], LoadDto.prototype, "payload", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], LoadDto.prototype, "pickup_address", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], LoadDto.prototype, "delivery_address", void 0);
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.IsNotEmptyObject)(),
    __metadata("design:type", Dimensions)
], LoadDto.prototype, "dimensions", void 0);
exports.LoadDto = LoadDto;
//# sourceMappingURL=load.dto.js.map