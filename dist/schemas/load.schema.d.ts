import * as mongoose from 'mongoose';
import { User } from './user.schema';
import { Dimensions } from '../dto/load.dto';
import { EnumStatus } from '../constants/enums';
export declare type LoadDocument = Load & mongoose.Document;
declare class Logs {
    message: string;
    time: Date;
}
export declare class Load {
    id: number;
    created_by: User;
    assigned_to: User;
    status: EnumStatus;
    state: null | 'En route to Pick Up' | 'Arrived to Pick Up' | 'En route to delivery' | 'Arrived to delivery';
    name: string;
    payload: number;
    pickup_address: string;
    delivery_address: string;
    dimensions: Dimensions;
    logs: Logs[];
    created_date: Date;
}
export declare const LoadSchema: mongoose.Schema<mongoose.Document<Load, any, any>, mongoose.Model<mongoose.Document<Load, any, any>, any, any, any>, {}>;
export {};
