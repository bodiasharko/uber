import * as mongoose from 'mongoose';
import { User } from './user.schema';
export declare type TruckDocument = Truck & mongoose.Document;
export declare class Truck {
    id: number;
    created_by: User;
    assigned_to: User;
    type: 'SPRINTER' | 'SMALL STRAIGHT' | 'LARGE STRAIGHT';
    status: 'OL' | 'IS';
    createdAt: Date;
}
export declare const TruckSchema: mongoose.Schema<mongoose.Document<Truck, any, any>, mongoose.Model<mongoose.Document<Truck, any, any>, any, any, any>, {}>;
