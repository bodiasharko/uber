import { NestMiddleware } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserDocument } from '../../schemas/user.schema';
export declare class IsUserExistMiddleware implements NestMiddleware {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    use(req: any, res: any, next: any): Promise<void>;
}
