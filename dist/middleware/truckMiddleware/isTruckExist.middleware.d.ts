import { NestMiddleware } from '@nestjs/common';
import { Model } from 'mongoose';
import { TruckDocument } from '../../schemas/truck.schema';
export declare class IsTruckExistMiddleware implements NestMiddleware {
    private notesModel;
    constructor(notesModel: Model<TruckDocument>);
    use(req: any, res: any, next: any): Promise<void>;
}
