import { NestMiddleware } from '@nestjs/common';
import { LoadDocument } from '../../schemas/load.schema';
import { Model } from 'mongoose';
export declare class IsLoadExistMiddleware implements NestMiddleware {
    private loadModel;
    constructor(loadModel: Model<LoadDocument>);
    use(req: any, res: any, next: any): Promise<void>;
}
