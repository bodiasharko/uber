"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsLoadNewMiddleware = void 0;
const common_1 = require("@nestjs/common");
const enums_1 = require("../../constants/enums");
const constants_1 = require("../../constants/constants");
class IsLoadNewMiddleware {
    use(req, res, next) {
        if (req.load.status !== enums_1.EnumStatus.NEW) {
            throw new common_1.BadRequestException({ message: constants_1.notNewLoad });
        }
        next();
    }
}
exports.IsLoadNewMiddleware = IsLoadNewMiddleware;
//# sourceMappingURL=IsLoadNew.middleware.js.map