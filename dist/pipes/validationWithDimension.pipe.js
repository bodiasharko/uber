"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationWithDimensionPipe = void 0;
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const constants_1 = require("../constants/constants");
const load_dto_1 = require("../dto/load.dto");
let ValidationWithDimensionPipe = class ValidationWithDimensionPipe {
    async transform(value, { metatype }) {
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = (0, class_transformer_1.plainToClass)(metatype, value);
        const dimensionsObject = (0, class_transformer_1.plainToClass)(load_dto_1.Dimensions, value.dimensions);
        const errors = await (0, class_validator_1.validate)(object);
        const dimensionsErrors = await (0, class_validator_1.validate)(dimensionsObject);
        if (errors.length > 0 || dimensionsErrors.length > 0) {
            throw new common_1.BadRequestException({ message: constants_1.invalidValidations });
        }
        return value;
    }
    toValidate(metatype) {
        const types = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }
};
ValidationWithDimensionPipe = __decorate([
    (0, common_1.Injectable)()
], ValidationWithDimensionPipe);
exports.ValidationWithDimensionPipe = ValidationWithDimensionPipe;
//# sourceMappingURL=validationWithDimension.pipe.js.map