export declare const ROLE_KEY = "role";
export declare const Roles: (...roles: string[]) => import("@nestjs/common").CustomDecorator<string>;
