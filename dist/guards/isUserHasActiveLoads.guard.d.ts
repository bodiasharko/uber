import { CanActivate } from '@nestjs/common';
import { LoadDocument } from '../schemas/load.schema';
import { Model } from 'mongoose';
export declare class IsUserHasActiveLoadsGuard implements CanActivate {
    private loadModel;
    constructor(loadModel: Model<LoadDocument>);
    canActivate(context: any): Promise<boolean>;
}
