import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserDocument } from '../../schemas/user.schema';
export declare class ChangePasswordGuard implements CanActivate {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
