"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsTruckAssignedGuard = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants/constants");
class IsTruckAssignedGuard {
    canActivate(context) {
        const { truck } = context.switchToHttp().getRequest();
        if (!truck.assigned_to) {
            return true;
        }
        throw new common_1.BadRequestException({ message: constants_1.truckAlreadyAssigned });
    }
}
exports.IsTruckAssignedGuard = IsTruckAssignedGuard;
//# sourceMappingURL=isTruckAssigned.guard.js.map