import { CanActivate, ExecutionContext } from '@nestjs/common';
export declare class IsTruckAssignedGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean;
}
