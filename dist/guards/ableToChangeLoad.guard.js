"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbleToChangeLoadGuard = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants/constants");
const enums_1 = require("../constants/enums");
class AbleToChangeLoadGuard {
    canActivate(context) {
        const { load } = context.switchToHttp().getRequest();
        if (load.status === enums_1.EnumStatus.NEW) {
            return true;
        }
        throw new common_1.BadRequestException({ message: constants_1.noChange });
    }
}
exports.AbleToChangeLoadGuard = AbleToChangeLoadGuard;
//# sourceMappingURL=ableToChangeLoad.guard.js.map