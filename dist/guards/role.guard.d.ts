import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserDocument } from '../schemas/user.schema';
import { Model } from 'mongoose';
export declare class RoleGuard implements CanActivate {
    private reflector;
    private userModel;
    constructor(reflector: Reflector, userModel: Model<UserDocument>);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
