import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Model } from 'mongoose';
import { LoadDocument } from '../schemas/load.schema';
export declare class BelongLoadToThisUserGuard implements CanActivate {
    private loadModel;
    constructor(loadModel: Model<LoadDocument>);
    canActivate(context: ExecutionContext): boolean;
}
