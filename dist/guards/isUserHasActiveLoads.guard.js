"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsUserHasActiveLoadsGuard = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const load_schema_1 = require("../schemas/load.schema");
const mongoose_2 = require("mongoose");
const enums_1 = require("../constants/enums");
const constants_1 = require("../constants/constants");
let IsUserHasActiveLoadsGuard = class IsUserHasActiveLoadsGuard {
    constructor(loadModel) {
        this.loadModel = loadModel;
    }
    async canActivate(context) {
        const { user } = context.switchToHttp().getRequest();
        const loads = await this.loadModel.find({ assigned_to: user.id });
        if (!loads.filter((load) => load.status !== enums_1.EnumStatus.SHIPPED).length) {
            return true;
        }
        throw new common_1.BadRequestException({ message: constants_1.finishLoad });
    }
};
IsUserHasActiveLoadsGuard = __decorate([
    __param(0, (0, mongoose_1.InjectModel)(load_schema_1.Load.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], IsUserHasActiveLoadsGuard);
exports.IsUserHasActiveLoadsGuard = IsUserHasActiveLoadsGuard;
//# sourceMappingURL=isUserHasActiveLoads.guard.js.map