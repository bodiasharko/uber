"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TruckInterceptor = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const truck_schema_1 = require("../schemas/truck.schema");
const mongoose_2 = require("mongoose");
const constants_1 = require("../constants/constants");
let TruckInterceptor = class TruckInterceptor {
    constructor(truckModel) {
        this.truckModel = truckModel;
    }
    async intercept(context, next) {
        try {
            const req = context.switchToHttp().getRequest();
            const [truck] = await this.truckModel.find({ assigned_to: req.user.id });
            req.truck = truck;
            return next.handle();
        }
        catch (e) {
            throw new common_1.BadRequestException({ message: constants_1.noUserTruck });
        }
    }
};
TruckInterceptor = __decorate([
    __param(0, (0, mongoose_1.InjectModel)(truck_schema_1.Truck.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], TruckInterceptor);
exports.TruckInterceptor = TruckInterceptor;
//# sourceMappingURL=truck.interceptor.js.map