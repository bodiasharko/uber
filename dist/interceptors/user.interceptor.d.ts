import { NestInterceptor } from '@nestjs/common';
import { UserDocument } from '../schemas/user.schema';
import { Model } from 'mongoose';
export declare class UserInterceptor implements NestInterceptor {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    intercept(context: any, next: any): Promise<any>;
}
