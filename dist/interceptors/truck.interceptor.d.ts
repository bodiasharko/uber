import { NestInterceptor } from '@nestjs/common';
import { TruckDocument } from '../schemas/truck.schema';
import { Model } from 'mongoose';
export declare class TruckInterceptor implements NestInterceptor {
    private truckModel;
    constructor(truckModel: Model<TruckDocument>);
    intercept(context: any, next: any): Promise<any>;
}
