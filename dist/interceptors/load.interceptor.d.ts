import { NestInterceptor } from '@nestjs/common';
import { Model } from 'mongoose';
import { LoadDocument } from '../schemas/load.schema';
export declare class LoadInterceptor implements NestInterceptor {
    private loadModel;
    constructor(loadModel: Model<LoadDocument>);
    intercept(context: any, next: any): Promise<any>;
}
