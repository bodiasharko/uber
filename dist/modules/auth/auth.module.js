"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const auth_controller_1 = require("./auth.controller");
const auth_service_1 = require("./auth.service");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("@nestjs/mongoose");
const jwt_1 = require("@nestjs/jwt");
const user_schema_1 = require("../../schemas/user.schema");
const isPasswordCorrect_middleware_1 = require("../../middleware/usersMiddleware/isPasswordCorrect.middleware");
const isUserRegistered_middleware_1 = require("../../middleware/usersMiddleware/isUserRegistered.middleware");
const isUserExist_middleware_1 = require("../../middleware/usersMiddleware/isUserExist.middleware");
let AuthModule = class AuthModule {
    configure(consumer) {
        consumer
            .apply(isUserExist_middleware_1.IsUserExistMiddleware)
            .forRoutes({ path: '/api/auth/register', method: common_1.RequestMethod.POST })
            .apply(isUserRegistered_middleware_1.IsUserRegisteredMiddleware, isPasswordCorrect_middleware_1.IsPasswordCorrectMiddleware)
            .forRoutes({ path: '/api/auth/login', method: common_1.RequestMethod.POST });
    }
};
AuthModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot(),
            mongoose_1.MongooseModule.forFeature([{ name: user_schema_1.User.name, schema: user_schema_1.UserSchema }]),
            jwt_1.JwtModule.register({
                secret: process.env.SECRET,
                signOptions: {
                    expiresIn: '24h',
                },
            }),
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [auth_service_1.AuthService],
        exports: [auth_service_1.AuthService, jwt_1.JwtModule],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map