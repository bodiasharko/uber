import { AuthService } from './auth.service';
import { UserDto } from '../../dto/user.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    register({ email, password, role }: UserDto): Promise<{
        message: string;
    }>;
    login({ email }: UserDto): Promise<{
        message: string;
        jwt_token: string;
    }>;
}
