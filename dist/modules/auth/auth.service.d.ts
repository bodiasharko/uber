import { UserDocument } from '../../schemas/user.schema';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
export declare class AuthService {
    private userModel;
    private jwtService;
    constructor(userModel: Model<UserDocument>, jwtService: JwtService);
    register(email: string, password: string, role: string): Promise<{
        message: string;
    }>;
    login(email: string): Promise<{
        message: string;
        jwt_token: string;
    }>;
}
