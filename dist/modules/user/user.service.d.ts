import { UserDocument } from '../../schemas/user.schema';
import { Model } from 'mongoose';
export declare class UserService {
    private userModel;
    constructor(userModel: Model<UserDocument>);
    getMe(id: number): Promise<{
        user: {
            _id: any;
            role: "SHIPPER" | "DRIVER";
            email: string;
            createdDate: Date;
        };
    }>;
    updatePassword(password: string, id: number): Promise<{
        message: string;
    }>;
    delete(id: number): Promise<{
        message: string;
    }>;
}
