import { UserService } from './user.service';
import { UpdatePasswordDto } from '../../dto/updatePassword.dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    get(req: any): Promise<{
        user: {
            _id: any;
            role: "SHIPPER" | "DRIVER";
            email: string;
            createdDate: Date;
        };
    }>;
    update({ newPassword }: UpdatePasswordDto, req: any): Promise<{
        message: string;
    }>;
    delete(req: any): Promise<{
        message: string;
    }>;
}
