"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TruckService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const truck_schema_1 = require("../../schemas/truck.schema");
const mongoose_2 = require("mongoose");
const constants_1 = require("../../constants/constants");
let TruckService = class TruckService {
    constructor(truckModel) {
        this.truckModel = truckModel;
    }
    async getAllTrucks(userId) {
        try {
            const trucks = await this.truckModel.find({ created_by: userId });
            return {
                trucks: trucks.map((truck) => ({
                    _id: truck._id,
                    created_by: truck.created_by,
                    assigned_to: truck.assigned_to,
                    type: truck.type,
                    status: truck.status,
                    created_date: truck.createdAt,
                })),
            };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async addNew(type, userId) {
        try {
            await this.truckModel.create({
                created_by: userId,
                assigned_to: null,
                type,
                status: 'IS',
            });
            return { message: constants_1.truckCreated };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getOneById(truck) {
        try {
            const { _id, created_by, assigned_to, type, status, createdAt } = truck;
            return {
                _id,
                created_by,
                assigned_to,
                type,
                status,
                created_date: createdAt,
            };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async updateById(truck, body) {
        try {
            await this.truckModel.findByIdAndUpdate(truck._id, Object.assign({}, body));
            return { message: constants_1.truckChanged };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async deleteById(truckId) {
        try {
            await this.truckModel.findByIdAndDelete(truckId);
            return { message: constants_1.truckDeleted };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async assign(truck, id) {
        try {
            const [oldTruck] = await this.truckModel.find({ assigned_to: id });
            if (oldTruck) {
                oldTruck.assigned_to = null;
                await oldTruck.save();
            }
            truck.assigned_to = id;
            await truck.save();
            return { message: constants_1.truckAssigned };
        }
        catch (e) {
            throw new common_1.BadRequestException({ message: constants_1.serverError });
        }
    }
};
TruckService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(truck_schema_1.Truck.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], TruckService);
exports.TruckService = TruckService;
//# sourceMappingURL=truck.service.js.map