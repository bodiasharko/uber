"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TruckController = void 0;
const common_1 = require("@nestjs/common");
const truck_service_1 = require("./truck.service");
const auth_guard_1 = require("../../guards/authGuard/auth.guard");
const role_decorator_1 = require("../../decorators/role.decorator");
const role_guard_1 = require("../../guards/role.guard");
const truckType_dto_1 = require("../../dto/truckType.dto");
const typeValidations_pipe_1 = require("../../pipes/typeValidations.pipe");
const enums_1 = require("../../constants/enums");
const isTruckAssigned_guard_1 = require("../../guards/isTruckAssigned.guard");
const isUserHasActiveLoads_guard_1 = require("../../guards/isUserHasActiveLoads.guard");
let TruckController = class TruckController {
    constructor(truckService) {
        this.truckService = truckService;
    }
    getAll(req) {
        return this.truckService.getAllTrucks(req.user.id);
    }
    addNew({ type }, req) {
        return this.truckService.addNew(type, req.user.id);
    }
    getById({ truck }) {
        return this.truckService.getOneById(truck);
    }
    putById({ truck }, body) {
        return this.truckService.updateById(truck, body);
    }
    deleteById({ truck }) {
        return this.truckService.deleteById(truck._id);
    }
    assign({ user, truck }) {
        return this.truckService.assign(truck, user.id);
    }
};
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "getAll", null);
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Post)(),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    (0, common_1.UsePipes)(new typeValidations_pipe_1.TypeValidationsPipe()),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [truckType_dto_1.TruckTypeDto, Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "addNew", null);
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "getById", null);
__decorate([
    (0, common_1.UseGuards)(isTruckAssigned_guard_1.IsTruckAssignedGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "putById", null);
__decorate([
    (0, common_1.UseGuards)(isTruckAssigned_guard_1.IsTruckAssignedGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "deleteById", null);
__decorate([
    (0, common_1.UseGuards)(isTruckAssigned_guard_1.IsTruckAssignedGuard),
    (0, common_1.UseGuards)(isUserHasActiveLoads_guard_1.IsUserHasActiveLoadsGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Post)(':id/assign'),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TruckController.prototype, "assign", null);
TruckController = __decorate([
    (0, common_1.Controller)('trucks'),
    __metadata("design:paramtypes", [truck_service_1.TruckService])
], TruckController);
exports.TruckController = TruckController;
//# sourceMappingURL=truck.controller.js.map