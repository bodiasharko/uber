import { TruckDocument } from '../../schemas/truck.schema';
import { Model } from 'mongoose';
export declare class TruckService {
    private truckModel;
    constructor(truckModel: Model<TruckDocument>);
    getAllTrucks(userId: any): Promise<{
        trucks: {
            _id: any;
            created_by: import("../../schemas/user.schema").User;
            assigned_to: import("../../schemas/user.schema").User;
            type: "SPRINTER" | "SMALL STRAIGHT" | "LARGE STRAIGHT";
            status: "OL" | "IS";
            created_date: Date;
        }[];
    }>;
    addNew(type: any, userId: any): Promise<{
        message: string;
    }>;
    getOneById(truck: any): Promise<{
        _id: any;
        created_by: any;
        assigned_to: any;
        type: any;
        status: any;
        created_date: any;
    }>;
    updateById(truck: any, body: any): Promise<{
        message: string;
    }>;
    deleteById(truckId: any): Promise<{
        message: string;
    }>;
    assign(truck: any, id: any): Promise<{
        message: string;
    }>;
}
