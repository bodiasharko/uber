import { TruckService } from './truck.service';
import { TruckTypeDto } from '../../dto/truckType.dto';
export declare class TruckController {
    private readonly truckService;
    constructor(truckService: TruckService);
    getAll(req: any): Promise<{
        trucks: {
            _id: any;
            created_by: import("../../schemas/user.schema").User;
            assigned_to: import("../../schemas/user.schema").User;
            type: "SPRINTER" | "SMALL STRAIGHT" | "LARGE STRAIGHT";
            status: "OL" | "IS";
            created_date: Date;
        }[];
    }>;
    addNew({ type }: TruckTypeDto, req: any): Promise<{
        message: string;
    }>;
    getById({ truck }: {
        truck: any;
    }): Promise<{
        _id: any;
        created_by: any;
        assigned_to: any;
        type: any;
        status: any;
        created_date: any;
    }>;
    putById({ truck }: {
        truck: any;
    }, body: any): Promise<{
        message: string;
    }>;
    deleteById({ truck }: {
        truck: any;
    }): Promise<{
        message: string;
    }>;
    assign({ user, truck }: {
        user: any;
        truck: any;
    }): Promise<{
        message: string;
    }>;
}
