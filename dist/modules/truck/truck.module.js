"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TruckModule = void 0;
const common_1 = require("@nestjs/common");
const truck_controller_1 = require("./truck.controller");
const truck_service_1 = require("./truck.service");
const auth_module_1 = require("../auth/auth.module");
const mongoose_1 = require("@nestjs/mongoose");
const user_schema_1 = require("../../schemas/user.schema");
const truck_schema_1 = require("../../schemas/truck.schema");
const isTruckExist_middleware_1 = require("../../middleware/truckMiddleware/isTruckExist.middleware");
const load_schema_1 = require("../../schemas/load.schema");
let TruckModule = class TruckModule {
    configure(consumer) {
        consumer
            .apply(isTruckExist_middleware_1.IsTruckExistMiddleware)
            .forRoutes({ path: '/api/trucks/:id', method: common_1.RequestMethod.ALL }, { path: '/api/trucks/:id/assign', method: common_1.RequestMethod.ALL });
    }
};
TruckModule = __decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            mongoose_1.MongooseModule.forFeature([{ name: load_schema_1.Load.name, schema: load_schema_1.LoadSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: truck_schema_1.Truck.name, schema: truck_schema_1.TruckSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: user_schema_1.User.name, schema: user_schema_1.UserSchema }]),
        ],
        controllers: [truck_controller_1.TruckController],
        providers: [truck_service_1.TruckService],
    })
], TruckModule);
exports.TruckModule = TruckModule;
//# sourceMappingURL=truck.module.js.map