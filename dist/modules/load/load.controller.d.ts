import { LoadService } from './load.service';
import { LoadDto } from '../../dto/load.dto';
export declare class LoadController {
    private readonly loadService;
    constructor(loadService: LoadService);
    getById({ user }: {
        user: any;
    }, { status, limit, offset }: {
        status: any;
        limit: any;
        offset: any;
    }): Promise<{
        loads: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        }[];
    }>;
    addNew(load: LoadDto, req: any): Promise<{
        message: string;
    }>;
    getActive({ load }: {
        load: any;
    }): Promise<{
        load: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        };
    }>;
    updateActive({ truck, load }: {
        truck: any;
        load: any;
    }): Promise<{
        message: string;
    }>;
    getLoadById({ load }: {
        load: any;
    }): {
        _id: any;
        created_by: any;
        assigned_to: any;
        status: any;
        state: any;
        name: any;
        payload: any;
        pickup_address: any;
        delivery_address: any;
        dimensions: any;
        logs: any;
        created_date: any;
    };
    updateLoadById({ load }: {
        load: any;
    }, body: any): Promise<{
        message: string;
    }>;
    deleteLoadById({ load }: {
        load: any;
    }): Promise<{
        message: string;
    }>;
    postLoadById({ load }: {
        load: any;
    }): Promise<{
        message: string;
        driver_found: boolean;
    }>;
    getFullInfo({ load }: {
        load: any;
    }): Promise<{
        load: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        };
        truck: {
            _id: any;
            created_by: any;
            assigned_to: any;
            type: any;
            status: any;
            created_date: any;
        };
    }>;
}
