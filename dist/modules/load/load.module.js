"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadModule = void 0;
const common_1 = require("@nestjs/common");
const load_controller_1 = require("./load.controller");
const load_service_1 = require("./load.service");
const auth_module_1 = require("../auth/auth.module");
const mongoose_1 = require("@nestjs/mongoose");
const load_schema_1 = require("../../schemas/load.schema");
const user_schema_1 = require("../../schemas/user.schema");
const IsLoadExist_middleware_1 = require("../../middleware/loadMiddleware/IsLoadExist.middleware");
const truck_schema_1 = require("../../schemas/truck.schema");
const IsLoadNew_middleware_1 = require("../../middleware/loadMiddleware/IsLoadNew.middleware");
let LoadModule = class LoadModule {
    configure(consumer) {
        consumer
            .apply(IsLoadExist_middleware_1.IsLoadExistMiddleware)
            .forRoutes({ path: '/api/loads/:id', method: common_1.RequestMethod.ALL }, { path: '/api/loads/:id/post', method: common_1.RequestMethod.ALL }, {
            path: '/api/loads/:id/shipping_info',
            method: common_1.RequestMethod.ALL,
        })
            .apply(IsLoadNew_middleware_1.IsLoadNewMiddleware)
            .forRoutes({ path: '/api/loads/:id/post', method: common_1.RequestMethod.POST });
    }
};
LoadModule = __decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            mongoose_1.MongooseModule.forFeature([{ name: load_schema_1.Load.name, schema: load_schema_1.LoadSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: user_schema_1.User.name, schema: user_schema_1.UserSchema }]),
            mongoose_1.MongooseModule.forFeature([{ name: truck_schema_1.Truck.name, schema: truck_schema_1.TruckSchema }]),
        ],
        controllers: [load_controller_1.LoadController],
        providers: [load_service_1.LoadService],
    })
], LoadModule);
exports.LoadModule = LoadModule;
//# sourceMappingURL=load.module.js.map