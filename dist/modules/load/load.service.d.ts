import { Model } from 'mongoose';
import { LoadDocument } from '../../schemas/load.schema';
import { TruckDocument } from '../../schemas/truck.schema';
export declare class LoadService {
    private loadModel;
    private truckModel;
    constructor(loadModel: Model<LoadDocument>, truckModel: Model<TruckDocument>);
    addNew(load: any, userId: any): Promise<{
        message: string;
    }>;
    getLoadById(load: any): {
        _id: any;
        created_by: any;
        assigned_to: any;
        status: any;
        state: any;
        name: any;
        payload: any;
        pickup_address: any;
        delivery_address: any;
        dimensions: any;
        logs: any;
        created_date: any;
    };
    updateLoadById(load: any, body: any): Promise<{
        message: string;
    }>;
    deleteById(id: any): Promise<{
        message: string;
    }>;
    getFullInfo(load: any): Promise<{
        load: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        };
        truck: {
            _id: any;
            created_by: any;
            assigned_to: any;
            type: any;
            status: any;
            created_date: any;
        };
    }>;
    postLoadById(load: any): Promise<{
        message: string;
        driver_found: boolean;
    }>;
    updateActive(truck: any, load: any): Promise<{
        message: string;
    }>;
    getActive(load: any): Promise<{
        load: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        };
    }>;
    getDriversList(id: any, status: any, limit?: number, offset?: number): Promise<{
        loads: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        }[];
    }>;
    getShipperList(id: any, status: any, limit?: number, offset?: number): Promise<{
        loads: {
            _id: any;
            created_by: any;
            assigned_to: any;
            status: any;
            state: any;
            name: any;
            payload: any;
            pickup_address: any;
            delivery_address: any;
            dimensions: any;
            logs: any;
            created_date: any;
        }[];
    }>;
}
