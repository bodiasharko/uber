"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const constants_1 = require("../../constants/constants");
const load_schema_1 = require("../../schemas/load.schema");
const enums_1 = require("../../constants/enums");
const truck_schema_1 = require("../../schemas/truck.schema");
const helpers_1 = require("../../constants/helpers");
let LoadService = class LoadService {
    constructor(loadModel, truckModel) {
        this.loadModel = loadModel;
        this.truckModel = truckModel;
    }
    async addNew(load, userId) {
        try {
            await this.loadModel.create(Object.assign(Object.assign({ created_by: userId, assigned_to: null, status: enums_1.EnumStatus.NEW, state: null }, load), { logs: [] }));
            return { message: constants_1.loadCreated };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    getLoadById(load) {
        try {
            return (0, helpers_1.formatLoad)(load);
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async updateLoadById(load, body) {
        try {
            await this.loadModel.findByIdAndUpdate(load._id, Object.assign({}, body));
            return { message: constants_1.loadUpdated };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async deleteById(id) {
        try {
            await this.loadModel.findByIdAndDelete(id);
            return { message: constants_1.loadDeleted };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getFullInfo(load) {
        try {
            const truck = await this.truckModel.findOne({
                assigned_to: load.assigned_to,
            });
            return (0, helpers_1.formatFullInfoLoad)(load, truck);
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async postLoadById(load) {
        try {
            await this.loadModel.findByIdAndUpdate(load._id, {
                status: enums_1.EnumStatus.POSTED,
            });
            const truckTypes = (0, helpers_1.findTruckType)(load.dimensions, load.payload);
            let trucks = await this.truckModel.find({ status: enums_1.EnumTruckStatus.IS });
            trucks = trucks.filter((truck) => !!truck.assigned_to && truckTypes.some((type) => type === truck.type));
            if (trucks.length) {
                trucks[0].status = enums_1.EnumTruckStatus.OL;
                trucks[0].save();
                load.assigned_to = trucks[0].assigned_to;
                load.status = enums_1.EnumStatus.ASSIGNED;
                load.state = enums_1.EnumState.ROUTE_TO;
                load.logs = [
                    ...load.logs,
                    {
                        message: `Load assigned to driver with id ${trucks[0].assigned_to}`,
                        time: new Date(Date.now()),
                    },
                ];
                load.save();
                return { message: constants_1.driverAssigned, driver_found: true };
            }
            await this.loadModel.findByIdAndUpdate(load._id, {
                status: enums_1.EnumStatus.NEW,
                logs: [
                    ...load.logs,
                    { message: constants_1.notAssigned, time: new Date(Date.now()) },
                ],
            });
            return { message: constants_1.noDriver, driver_found: false };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async updateActive(truck, load) {
        try {
            switch (load.state) {
                case enums_1.EnumState.ROUTE_TO:
                    load.state = enums_1.EnumState.ARRIVED;
                    load.logs = [
                        ...load.logs,
                        {
                            message: enums_1.EnumState.ARRIVED,
                            time: new Date(Date.now()),
                        },
                    ];
                    break;
                case enums_1.EnumState.ARRIVED:
                    load.state = enums_1.EnumState.DELIVERY;
                    load.logs = [
                        ...load.logs,
                        {
                            message: enums_1.EnumState.DELIVERY,
                            time: new Date(Date.now()),
                        },
                    ];
                    break;
                case enums_1.EnumState.DELIVERY:
                    load.state = enums_1.EnumState.FINISH;
                    load.status = enums_1.EnumStatus.SHIPPED;
                    load.logs = [
                        ...load.logs,
                        {
                            message: enums_1.EnumState.FINISH,
                            time: new Date(Date.now()),
                        },
                    ];
                    truck.status = enums_1.EnumTruckStatus.IS;
                    break;
                default:
                    break;
            }
            await load.save();
            await truck.save();
            return { message: `Load state changed to '${load.state}'` };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getActive(load) {
        try {
            return { load: (0, helpers_1.formatLoad)(load) };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getDriversList(id, status, limit = 10, offset = 0) {
        try {
            let fetchLoads;
            if (status) {
                fetchLoads = await this.loadModel.find({ assigned_to: id, status });
            }
            else {
                fetchLoads = await this.loadModel.find({ assigned_to: id });
            }
            let loads = [...fetchLoads];
            loads = loads.splice(offset).splice(0, limit);
            return { loads: loads.map((load) => (0, helpers_1.formatLoad)(load)) };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
    async getShipperList(id, status, limit = 10, offset = 0) {
        try {
            let fetchLoads;
            if (status) {
                fetchLoads = await this.loadModel.find({ created_by: id, status });
            }
            else {
                fetchLoads = await this.loadModel.find({ created_by: id });
            }
            let loads = [...fetchLoads];
            loads = loads.splice(offset).splice(0, limit);
            return { loads: loads.map((load) => (0, helpers_1.formatLoad)(load)) };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException({ message: constants_1.serverError });
        }
    }
};
LoadService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(load_schema_1.Load.name)),
    __param(1, (0, mongoose_1.InjectModel)(truck_schema_1.Truck.name)),
    __metadata("design:paramtypes", [mongoose_2.Model,
        mongoose_2.Model])
], LoadService);
exports.LoadService = LoadService;
//# sourceMappingURL=load.service.js.map