"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadController = void 0;
const common_1 = require("@nestjs/common");
const load_service_1 = require("./load.service");
const role_guard_1 = require("../../guards/role.guard");
const auth_guard_1 = require("../../guards/authGuard/auth.guard");
const role_decorator_1 = require("../../decorators/role.decorator");
const load_dto_1 = require("../../dto/load.dto");
const validationWithDimension_pipe_1 = require("../../pipes/validationWithDimension.pipe");
const enums_1 = require("../../constants/enums");
const belongLoadToThisUser_guard_1 = require("../../guards/belongLoadToThisUser.guard");
const truck_interceptor_1 = require("../../interceptors/truck.interceptor");
const load_interceptor_1 = require("../../interceptors/load.interceptor");
const ableToChangeLoad_guard_1 = require("../../guards/ableToChangeLoad.guard");
let LoadController = class LoadController {
    constructor(loadService) {
        this.loadService = loadService;
    }
    getById({ user }, { status, limit, offset }) {
        if (user.role === enums_1.EnumRole.DRIVER) {
            return this.loadService.getDriversList(user.id, status, limit, offset);
        }
        if (user.role === enums_1.EnumRole.SHIPPER) {
            return this.loadService.getShipperList(user.id, status, limit, offset);
        }
    }
    addNew(load, req) {
        return this.loadService.addNew(load, req.user.id);
    }
    getActive({ load }) {
        return this.loadService.getActive(load);
    }
    updateActive({ truck, load }) {
        return this.loadService.updateActive(truck, load);
    }
    getLoadById({ load }) {
        return this.loadService.getLoadById(load);
    }
    updateLoadById({ load }, body) {
        return this.loadService.updateLoadById(load, body);
    }
    deleteLoadById({ load }) {
        return this.loadService.deleteById(load._id);
    }
    postLoadById({ load }) {
        return this.loadService.postLoadById(load);
    }
    getFullInfo({ load }) {
        return this.loadService.getFullInfo(load);
    }
};
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER, enums_1.EnumRole.DRIVER),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "getById", null);
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER),
    (0, common_1.Post)(),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    (0, common_1.UsePipes)(new validationWithDimension_pipe_1.ValidationWithDimensionPipe()),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [load_dto_1.LoadDto, Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "addNew", null);
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Get)('active'),
    (0, common_1.UseInterceptors)(load_interceptor_1.LoadInterceptor),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "getActive", null);
__decorate([
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.DRIVER),
    (0, common_1.Patch)('active/state'),
    (0, common_1.UseInterceptors)(truck_interceptor_1.TruckInterceptor, load_interceptor_1.LoadInterceptor),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "updateActive", null);
__decorate([
    (0, common_1.UseGuards)(belongLoadToThisUser_guard_1.BelongLoadToThisUserGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "getLoadById", null);
__decorate([
    (0, common_1.UseGuards)(ableToChangeLoad_guard_1.AbleToChangeLoadGuard),
    (0, common_1.UseGuards)(belongLoadToThisUser_guard_1.BelongLoadToThisUserGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER),
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "updateLoadById", null);
__decorate([
    (0, common_1.UseGuards)(ableToChangeLoad_guard_1.AbleToChangeLoadGuard),
    (0, common_1.UseGuards)(belongLoadToThisUser_guard_1.BelongLoadToThisUserGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "deleteLoadById", null);
__decorate([
    (0, common_1.UseGuards)(belongLoadToThisUser_guard_1.BelongLoadToThisUserGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER),
    (0, common_1.Post)(':id/post'),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "postLoadById", null);
__decorate([
    (0, common_1.UseGuards)(belongLoadToThisUser_guard_1.BelongLoadToThisUserGuard),
    (0, common_1.UseGuards)(role_guard_1.RoleGuard),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, role_decorator_1.Roles)(enums_1.EnumRole.SHIPPER),
    (0, common_1.Get)(':id/shipping_info'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], LoadController.prototype, "getFullInfo", null);
LoadController = __decorate([
    (0, common_1.Controller)('loads'),
    __metadata("design:paramtypes", [load_service_1.LoadService])
], LoadController);
exports.LoadController = LoadController;
//# sourceMappingURL=load.controller.js.map