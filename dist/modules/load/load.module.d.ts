import { MiddlewareConsumer } from '@nestjs/common';
export declare class LoadModule {
    configure(consumer: MiddlewareConsumer): any;
}
