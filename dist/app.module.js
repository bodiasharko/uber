"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
const mongoose_1 = require("@nestjs/mongoose");
const nest_morgan_1 = require("nest-morgan");
const auth_module_1 = require("./modules/auth/auth.module");
const user_module_1 = require("./modules/user/user.module");
const load_module_1 = require("./modules/load/load.module");
const truck_module_1 = require("./modules/truck/truck.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot(),
            mongoose_1.MongooseModule.forRoot(process.env.DATABASE_URL),
            core_1.RouterModule.register([
                {
                    path: '/api',
                    module: auth_module_1.AuthModule,
                },
                {
                    path: '/api',
                    module: user_module_1.UserModule,
                },
                {
                    path: '/api',
                    module: truck_module_1.TruckModule,
                },
                {
                    path: '/api',
                    module: load_module_1.LoadModule,
                },
            ]),
            nest_morgan_1.MorganModule,
            auth_module_1.AuthModule,
            user_module_1.UserModule,
            load_module_1.LoadModule,
            truck_module_1.TruckModule,
        ],
        providers: [
            {
                provide: core_1.APP_INTERCEPTOR,
                useClass: (0, nest_morgan_1.MorganInterceptor)('combined'),
            },
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map